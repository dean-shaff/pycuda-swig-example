#ifndef __sum_hpp
#define __sum_hpp

namespace example {

  template<typename Type>
  Type sum (Type* arr, unsigned size);

}



#endif
