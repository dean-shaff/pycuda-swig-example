#include <iostream>

#include "example/sum.hpp"

#define FULL_MASK 0xffffffff

namespace example {

  template<typename Type>
  __device__ Type warp_reduce (Type val)
  {
    // offset >>= 1 is equivalent to offset /= 2
    for (unsigned offset = warpSize/2; offset > 0; offset >>= 1) {
      val += __shfl_down_sync(FULL_MASK, val, offset);
    }
    return val;
  }


  template<typename Type>
  __global__ void sum_warp_reduce (
    Type* arr,
    unsigned size
  )
  {
    extern __shared__ unsigned char _scratch[];
    Type* scratch = reinterpret_cast<Type*>(_scratch);

    Type partial_sum = 0;
    for (unsigned idx=(blockDim.x*blockIdx.x + threadIdx.x); idx < size; idx+=(blockDim.x*gridDim.x)) {
      partial_sum += arr[idx];
    }

    partial_sum = warp_reduce<Type>(partial_sum);

    unsigned warp_lane = threadIdx.x % warpSize;
    unsigned warp_idx = threadIdx.x / warpSize;

    if (warp_lane == 0) {
      scratch[warp_idx] = partial_sum;
    }

    __syncthreads();

    // now sum up scratch space

    if (warp_idx == 0) {
      partial_sum = scratch[warp_lane];

      partial_sum = warp_reduce(partial_sum);

      if (warp_lane == 0) {
        arr[blockIdx.x] = partial_sum;
      }
    }
  }

  template<typename Type>
  Type sum (Type* arr, unsigned size) {

    unsigned nblocks = size / 1024;
    if (nblocks == 0) {
      nblocks++;
    }
    std::cerr << "example::sum: nblocks=" << nblocks << std::endl;

    sum_warp_reduce<Type><<<nblocks, 1024, 32*sizeof(Type)>>>(arr, size);
    sum_warp_reduce<Type><<<1, 1024, 32*sizeof(Type)>>>(arr, nblocks);

    Type result;

    cudaMemcpy(&result, arr, sizeof(Type), cudaMemcpyDeviceToHost);

    return result;

  }

  template float sum<float> (float* arr, unsigned size);
  template double sum<double> (double* arr, unsigned size);

}
