#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

#include <cuda_runtime.h>

#include "example/sum.hpp"

int main () {
  unsigned size = 53735;
  double result_expected = 1443698245.0;


  std::vector<double> in(size);
  double* in_d;

  for (unsigned idx=0; idx<size; idx++) {
    in[idx] = (double) idx;
  }

  cudaMalloc(&in_d, size*sizeof(double));
  cudaMemcpy(in_d, in.data(), size*sizeof(double), cudaMemcpyHostToDevice);

  double result = example::sum<double>(in_d, size);

  std::cerr.precision(10);
  std::cerr << "result=" << result << ", expected result=" << result_expected << std::endl;
  std::cerr << "result - expected result = " << std::abs(result_expected - result) << std::endl;

  cudaFree(in_d);

  return 0;
}
