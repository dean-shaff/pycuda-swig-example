import unittest
import os
import sys

import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray


cur_dir = os.path.dirname(os.path.abspath(__file__))
build_python_dir = os.path.join(
    os.path.dirname(cur_dir),
    "build",
    "python"
)
sys.path.append(build_python_dir)

import example


class TestExample(unittest.TestCase):

    def test_sum(self):

        size = 53735
        d_arr = gpuarray.arange(size, dtype=np.float64)
        h_arr = d_arr.get()
        expected = np.sum(h_arr)

        test = example.sum(d_arr)
        self.assertTrue(np.isclose(expected, test))


if __name__ == "__main__":
    unittest.main()
