%module example

%{

#include "example/sum.hpp"

long pycuda_get_ptr (PyObject* obj) {
  return PyLong_AsLongLong(
    PyObject_GetAttrString(obj, "ptr")
  );
}

long long pycuda_get_size (PyObject* obj) {
  return (unsigned) PyLong_AsLong(
      PyTuple_GetItem(
        PyObject_GetAttrString(obj, "shape"), 0));
}

%}

%include "example/sum.hpp"

%inline %{
  namespace example {
    template<typename Type>
    PyObject* sum (PyObject*);
  }
%}

%template(sum_float) example::sum<float>;
%template(sum_double) example::sum<double>;


%define CUDAExtend(Type)
%inline %{
namespace example {

  template<>
  PyObject* sum<Type> (PyObject* in) {
    long long in_ptr = pycuda_get_ptr(in);
    long long in_size = pycuda_get_size(in);
    Type result = example::sum<Type>(
      reinterpret_cast<Type*>(in_ptr),
      (unsigned) in_size
    );
    return PyFloat_FromDouble((double) result);
  }
}
%}
%enddef

CUDAExtend(float)
CUDAExtend(double)

%pythoncode %{

import numpy as np

def sum(in_array):
  if in_array.dtype == np.float32:
    return sum_float(in_array)
  elif in_array.dtype == np.float64:
    return sum_double(in_array)


%}
